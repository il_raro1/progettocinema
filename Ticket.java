class Ticket {
    private Film film;
    private boolean acquistato;
    private double prezzo;

    public Ticket(Film film) {
        this.film = film;
        this.prezzo = film.getPrezzoBiglietto();
        this.acquistato = false;
    }

    public Film getFilm() {
        return film;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void acquista() {
        if (!acquistato) {
          
            acquistato = true;
        } else {
            System.out.println("Il biglietto è già stato acquistato.");
        }
    }
    }
