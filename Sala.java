import java.util.ArrayList;
import java.util.List;

public class Sala {
    private int numeroSala;
    private int postiTotali;
    private int postiOccupati;
    private List<Film> elencoFilm;

    public Sala(int numeroSala, int postiTotali) {
        this.numeroSala = numeroSala;
        this.postiTotali = postiTotali;
        this.postiOccupati = 0;
        this.elencoFilm = new ArrayList<>();
    }

    public int getNumeroSala() {
        return numeroSala;
    }

    public int getPostiLiberi() {
        return postiTotali - postiOccupati;
    }

    public boolean prenotaPosti(int numeroPosti) {
        if (getPostiLiberi() >= numeroPosti) {
            postiOccupati += numeroPosti;
            return true;
        } else {
            System.out.println("Posti non disponibili.");
            return false;
        }
    }

    public int getPostiOccupati() {
        return postiOccupati;
    }

    public void aggiungiFilm(Film film) {
        elencoFilm.add(film);
        film.aggiungiSala(this); 
    }

    public boolean haFilm(Film film) {
        return elencoFilm.contains(film);
    }

    public List<Film> getElencoFilm() {
        return elencoFilm;
    }
}
