import java.util.ArrayList;

class Utente {
    private String nome;
    private String cognome;
    private double fondi;
    private int eta;
    private ArrayList<Ticket> biglietti;

    public Utente(String nome, String cognome, double fondi, int eta) {
        this.biglietti = new ArrayList<>();
        this.nome = nome;
        this.cognome = cognome;
        this.fondi = fondi;
        this.eta = eta;
    }

    public void spendiFondi(double importo) {
        fondi -= importo;
    }

    public double getFondi() {
        return fondi;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void aggiungiBiglietto(Ticket ticket) {
        biglietti.add(ticket);
    }

    public void sottraiFondi(double importo) {
        fondi -= importo;
    }

    public void stampaRecapBigliettiAcquistati() {
        System.out.println("Recap dei biglietti acquistati:");
        for (Ticket ticket : biglietti) {
            System.out.println("Film: " + ticket.getFilm().getTitolo());
            System.out.println("Prezzo: " + ticket.getPrezzo() + " euro");
            System.out.println("------------------------");
        }
    }
}