# ProgettoCinema

Realizzato da Alexander Rarità - Paolo Ciprani - Massimiliano Mazza

Il progetto è un sistema di gestione di un cinema con funzionalità di vendita di biglietti, associazione di film alle sale, e gestione di un negozio interno. Ecco alcune delle principali funzionalità:

 GESTIONE UTENTE:
   - L'utente può inserire il proprio nome, cognome, età e fondi disponibili.
   - Viene verificato se l'utente è maggiorenne; in caso contrario, il servizio non è disponibile.

 CREAZIONE CINEMA E SALE:
   - Viene creato un cinema con una lista di sale.
   - Ogni sala ha un numero identificativo e una capacità massima di posti.

 ASSOCIAZIONE FILM ALLE SALE:
   - Film diversi vengono associati a sale specifiche del cinema.

 VISUALIZZAZIONE FILM DISPONIBILI:
   - Viene mostrata una lista di film disponibili nel cinema con dettagli come titolo, regista, anno e prezzo del biglietto.

 ACQUISTO BIGLIETTI: 
   - L'utente può selezionare un film dalla lista e specificare la quantità di biglietti desiderata.
   - Il sistema verifica la disponibilità di posti e la validità dell'acquisto in base ai fondi dell'utente.

 NEGOZIO INTERNO:
   - Dopo l'acquisto dei biglietti, l'utente può accedere a un negozio interno.
   - Può acquistare prodotti dal negozio, che ridurranno i fondi disponibili.

 STAMPA RECAP BIGLIETTI ACQUISTATI:
   - Viene visualizzato un recap dei biglietti acquistati dall'utente.

TESTARE IL PROGRAMMA:
Per testare il programma, si può eseguire il codice su ambiente java o eseguire da terminale il makefile fornito anch'esso. L'utente può inserire le proprie informazioni, scegliere un film, acquistare i biglietti e accedere al negozio interno.
