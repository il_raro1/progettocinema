import java.util.ArrayList;

class Cinema {
    private ArrayList<Sala> sale;
    private Shop shop;
    private ArrayList<Ticket> tickets;
    private ArrayList<Utente> utenti;
    private double fondoAzienda;

    public Cinema() {
        this.sale = new ArrayList<>();
        this.shop = new Shop();
        this.tickets = new ArrayList<>();
        this.utenti = new ArrayList<>();
        this.fondoAzienda = 0.0;
    }

    public void aggiungiSala(Sala sala) {
        sale.add(sala);
    }

    private Sala getSalaDisponibile(Film film, int numeroPosti) {
        for (Sala sala : sale) {
            if (sala.haFilm(film) && sala.getPostiLiberi() >= numeroPosti) {
                return sala;
            }
        }
        return null;
    }

    public void vendiBiglietto(Film film, Utente utente, int quantita) {
  
        Sala salaDisponibile = getSalaDisponibile(film, quantita);
        if (salaDisponibile == null) {
            System.out.println("Il film selezionato non è disponibile o non ci sono posti a sufficienza.");
            return;
        }

        double costoTotale = film.getPrezzoBiglietto() * quantita;

        if (utente.getFondi() < costoTotale) {
            System.out.println("Fondi insufficienti per acquistare i biglietti.");
            return;
        }

        if (salaDisponibile.prenotaPosti(quantita)) {
            for (int i = 0; i < quantita; i++) {
                Ticket ticket = new Ticket(film); 
                utente.aggiungiBiglietto(ticket);
                tickets.add(ticket); 
            }
        }

        utente.sottraiFondi(costoTotale);

        System.out.println("Biglietti acquistati con successo!");
        System.out.println("Totale speso: " + costoTotale + " euro.");
    }

    private boolean filmDisponibile(Film film) {
        for (Sala sala : sale) {
            if (sala.haFilm(film) && sala.getPostiLiberi() > 0) {
                return true;
            }
        }
        return false;
    }

    private Sala trovaSalaDisponibile(Film film, int numeroPosti) {
        for (Sala sala : sale) {
            if (sala.getPostiLiberi() >= numeroPosti) {
                return sala;
            }
        }
        return null;
    }

    public ArrayList<Film> getElencoFilm() {
        ArrayList<Film> elencoFilm = new ArrayList<>();
        for (Sala sala : sale) {
            elencoFilm.addAll(sala.getElencoFilm());
        }
        return elencoFilm;
    }

    public void stampaFilm() {
        ArrayList<Film> elencoFilm = getElencoFilm();
        for (Film film : elencoFilm) {
            System.out.println(film.getNumero() + ". " + film.getTitolo() + " di " + film.getRegista() +
                    " (" + film.getAnno() + "), Prezzo: " + film.getPrezzoBiglietto() + " euro, Sala: " + film.getSala().getNumeroSala());
        }
    }
    
    public Film getFilmByNumber(int numeroFilm) {
        ArrayList<Film> elencoFilm = getElencoFilm();
        for (Film film : elencoFilm) {
            if (film.getNumero() == numeroFilm) {
                return film;
            }
        }
        return null;
    }
    
    public Sala getSalaByNumber(int numeroSala) {
        for (Sala sala : sale) {
            if (sala.getNumeroSala() == numeroSala) {
                return sala;
            }
        }
        return null;
    }
   
    public void aggiungiFilmASala(int numeroSala, Film film) {
        Sala sala = getSalaByNumber(numeroSala);
        if (sala != null) {
            sala.aggiungiFilm(film);
            film.setSala(sala);
        }
    }  
}
