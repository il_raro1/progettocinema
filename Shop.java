import java.util.ArrayList;
import java.util.Scanner;

class Shop {
    private double fondiNegozio;
    private double prezzoBibita = 3.0;
    private double prezzoAcqua = 1.0;
    private double prezzoPopcorn = 2.5;
    private double prezzoPatatine = 2.0;
    private ArrayList<Prodotto> prodottiAcquistati;

    public Shop() {
        this.fondiNegozio = 1000.0;
        this.prodottiAcquistati = new ArrayList<>();
    }

    public void acquistaProdotti(Utente utente) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Benvenuto nel negozio!");

        while (true) {
            System.out.println("1. Acquista bevande");
            System.out.println("2. Acquista cibo");
            System.out.println("3. Esci dal negozio");
            System.out.print("Scelta: ");

            int scelta = scanner.nextInt();

            switch (scelta) {
                case 1:
                    System.out.println("Seleziona la bevanda:");
                    System.out.println("1. Bibita 1.5L - " + prezzoBibita + "€");
                    System.out.println("2. Acqua 0.5L - " + prezzoAcqua + "€");
                    int sceltaBevanda = scanner.nextInt();
                    double prezzoBevanda = (sceltaBevanda == 1) ? prezzoBibita : prezzoAcqua;
                    acquistaProdotto(utente, prezzoBevanda, "Bevanda");
                    break;

                case 2:
                    System.out.println("Seleziona il cibo:");
                    System.out.println("1. Popcorn - " + prezzoPopcorn + "€");
                    System.out.println("2. Patatine - " + prezzoPatatine + "€");
                    int sceltaCibo = scanner.nextInt();
                    double prezzoCibo = (sceltaCibo == 1) ? prezzoPopcorn : prezzoPatatine;
                    acquistaProdotto(utente, prezzoCibo, "Cibo");
                    break;

                case 3:
                    System.out.println("Grazie per gli acquisti!");
                    stampaRecapAcquisti();
                    return;

                default:
                    System.out.println("Scelta non valida. Riprova.");
            }
        }
    }

    private void acquistaProdotto(Utente utente, double prezzo, String nomeProdotto) {
        if (utente.getFondi() >= prezzo) {
            utente.spendiFondi(prezzo);
            fondiNegozio += prezzo;
            Prodotto articoloAcquistato = new Prodotto(nomeProdotto, prezzo);
            prodottiAcquistati.add(articoloAcquistato);
            System.out.println(nomeProdotto + " acquistato con successo!");
        } else {
            System.out.println("Fondi insufficienti.");
        }
    }

    private void stampaRecapAcquisti() {
        System.out.println("Recap acquisti:");
        for (Prodotto prodotto : prodottiAcquistati) {
            System.out.println("Prodotto: " + prodotto.getNome() + ", Prezzo: " + prodotto.getPrezzo() + "€");
        }
    }
}
