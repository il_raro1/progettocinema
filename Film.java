import java.util.ArrayList;
import java.util.List;

public class Film {
    private String titolo;
    private String regista;
    private int anno;
    private double prezzoBiglietto;
    private static int prossimoNumero = 1;
    private int numero;
    private List<Sala> saleAssociate;

    public Film(String titolo, String regista, int anno, double prezzoBiglietto) {
        this.titolo = titolo;
        this.regista = regista;
        this.anno = anno;
        this.prezzoBiglietto = prezzoBiglietto;
        this.numero = prossimoNumero++;
        this.saleAssociate = new ArrayList<>();
    }

    public int getAnno() {
        return anno;
    }

    public String getTitolo() {
        return titolo;
    }

    public String getRegista() {
        return regista;
    }

    public double getPrezzoBiglietto() {
        return prezzoBiglietto;
    }

    public List<Sala> getSaleAssociate() {
        return saleAssociate;
    }

    public void aggiungiSala(Sala sala) {
        saleAssociate.add(sala);
    }

    public int getNumero() {
        return numero;
    }

   
    public Sala getSala() {
        if (!saleAssociate.isEmpty()) {
            return saleAssociate.get(0);  
        }
        return null;
    }

  
    public void setSala(Sala sala) {
        saleAssociate.clear();
        saleAssociate.add(sala);
    }
}
