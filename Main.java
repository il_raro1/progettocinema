import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Inserisci il tuo nome: ");
        String nome = scanner.nextLine();
        System.out.print("Inserisci il tuo cognome: ");
        String cognome = scanner.nextLine();
        System.out.print("Inserisci la tua età: ");
        int eta = scanner.nextInt();

        
        if (eta < 14) {
            System.out.println("Mi dispiace, ma il servizio è disponibile solo per età pari o maggiore di 14 anni.");
            return;
        }

        System.out.print("Inserisci i tuoi fondi: ");
        double fondi = scanner.nextDouble();

        
        Utente utente = new Utente(nome, cognome, fondi, eta);
        System.out.println("Benvenuto, " + utente.getNome() + " " + utente.getCognome() + "!");

        
        Cinema cinema = new Cinema();
        Sala sala1 = new Sala(1, 150);
        Sala sala2 = new Sala(2, 150);
        Sala sala3 = new Sala(3, 150);
        Sala sala4 = new Sala(4, 150);
        Sala sala5 = new Sala(5, 150);
        cinema.aggiungiSala(sala1);
        cinema.aggiungiSala(sala2);
        cinema.aggiungiSala(sala3);
        cinema.aggiungiSala(sala4);
        cinema.aggiungiSala(sala5);

        associaFilmASale(cinema);

        System.out.println("Lista film disponibili:");
        cinema.stampaFilm();

        System.out.print("Inserisci il numero del film scelto: ");
        int sceltaFilm = scanner.nextInt();
        System.out.print("Inserisci la quantità di biglietti desiderati: ");
        int quantitaBiglietti = scanner.nextInt();

        Film filmScelto = cinema.getFilmByNumber(sceltaFilm);

        if (filmScelto != null) {

            cinema.vendiBiglietto(filmScelto, utente, quantitaBiglietti);

            Shop shop = new Shop();
            shop.acquistaProdotti(utente);

            utente.stampaRecapBigliettiAcquistati();
        } else {
            System.out.println("Scelta del film non valida.");
        }
    }

    private static void associaFilmASale(Cinema cinema) {
        cinema.aggiungiFilmASala(1, new Film("Oppenheimer", "Christopher Nolan", 2023, 10.0));
        cinema.aggiungiFilmASala(2, new Film("In the Mood for Love (Huāyàng niánhuá)", "Wong Kar-wai", 2000, 10.0));
        cinema.aggiungiFilmASala(3, new Film("Il petroliere (There Will Be Blood)", "Paul Thomas Anderson", 2007, 10.0));
        cinema.aggiungiFilmASala(4, new Film("La città incantata (Sen to Chihiro no kamikakushi)", "Hayao Miyazaki", 2001, 10.0));
        cinema.aggiungiFilmASala(5, new Film("Boyhood", "Richard Linklater", 2014, 10.0));

    }
    
}